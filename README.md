# GE-GJ algorithm

## Name
Gaussian Elimination and Gauss-Jordan Elimination Algorithms.

## Description
Both are algorithms for solving systems of linear equations. Some sequence of operations are performed on matrix of coefficients. They are also known as row reduction and can be used for compute the determinant of the matrix or the inverse of an invertible matrix.
- **Gaussian Elimination**:
        Performing operations (swapping 2 rows, multiplying a row by a non-scalar, adding to one row a multiple of another) for converting the matrix into an upper triangular matrix in row echelon form. AFter reduction to row echelon form we can see if there are o solutions/unique solution or infinitely many solutions.

        Algorithm:
         1. Partial pivoting: Find the k-th pivot by swapping rows, to move the entry with the largest absolute value to the pivot position. This imparts computational stability to the algorithm.
         2. For each row below the pivot, calculate the factor f which makes the kth entry zero, and for every element in the row subtract the fth multiple of the corresponding element in the kth row.
         3. Repeat above steps for each unknown. We will be left with a partial row echelon form matrix.

        Time Complexity: Since for each pivot we traverse the part to its right for each row below it, O(n)*(O(n)*O(n)) = O(n3).

- **Gauss-Jordan Elimination**:
        A modified version of Gauss Elimination method, in which we only have to form a reduced row echelon form (diagonal matrix).

## Usage
```
gcc GE.c -o GE
./GE 2048 
```
(where 2048 is the dimension of the square matrix)

## References

1. [https://www.geeksforgeeks.org/gaussian-elimination/](url)
2. [https://www.geeksforgeeks.org/program-for-gauss-jordan-elimination-method/](url)
3. [https://www.studypug.com/algebra-help/solving-a-linear-system-with-matrices-using-gaussian-elimination](url)




