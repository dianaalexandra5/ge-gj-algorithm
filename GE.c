#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <math.h>

void printa(float **a, int len){
     int i, j;
     for(i = 0; i < len; i++)  {
           for (j = 0; j <= len; j++)
                printf("%f  ", a[i][j]);
           printf("\n");
     }
     printf("\n");
}

void printx(float *x, int len){
     int i;
     for (i = 0; i < len; i++)
         printf("%f  ", x[i]);
     printf("\n");
}

int main(int argc, char **argv) 
{
 int n = 1024; // dimensiunea matricei, n se introduce din linia de comanda
 int i, j, k, r; 
 float max;
 double sum;
 struct timeval t1, t2;	
    float tp;   
    char *fn = "Res_GE.txt";	    
    FILE *fp;

 // Citire parametru din linia de comanda
 if (argc >= 2) n = atoi(argv[1]);

 // Alocare date
 float *x = (float*)malloc(sizeof(float)*n);
 float *pivot = (float*)malloc(sizeof(float)*(n+1));
 float **a = (float**)malloc(sizeof(float*)*n);

 for (i = 0; i < n; i++)
 a[i] = (float*)malloc(sizeof(float)*(n+1));

 // Initializare matrice aleator
 for (i = 0; i < n ;i++)
   	for(j = 0; j <= n; j++)
           a[i][j] = rand()/(float)RAND_MAX;

gettimeofday(&t1, NULL);
 // Prima etapa: reducerea matricei A|B
 for (k = 0; k < n; k++){
// Selectia liniei pivot (linia r)
max = 0; // Valoarea elem. pivot maxim
for (i = k; i < n; i++) 
     if(abs(a[i][k]) > max){max = abs(a[i][k]); r = i; }

// Interschimb intre liniile r si k
for (j = k; j < n; j++) {
     pivot[j] = a[r][j]; // Se fol. vector pivot[n+1]
     a[r][j] = a[k][j]; a[k][j] = pivot[j];
}
 // Scalarea liniei pivot k
for (j = k+1; j <= n; j++) 
     a[k][j] = a[k][j]/a[k][k]; 

a[k][k] = 1;
 // Reducerea liniilor submatricei active
for (i = k+1; i < n; i++) {
     // Reducerea liniei i 
     for(j = k+1; j <= n; j++) 
          a[i][j] -= a[i][k]*a[k][j];
          a[i][k] = 0; 
     }
 }
 // A doua etapa – calculul solutiilor prin substitutie inapoi 
 // Insumarea termenilor ecuatiei se face in variabila sum 
 for (i = n-1 ; i >= 0; i--){
     sum = 0; 
     for (j = i+1; j < n; j++) 
          sum += a[i][j]*x[j];

     x[i] = a[i][n] - sum;
 }
 
 gettimeofday(&t2, NULL);
    tp = (float) (t2.tv_sec - t1.tv_sec) + 0.000001*(t2.tv_usec - t1.tv_usec);
	
    // Scriere in fisier
    fp = fopen(fn, "a");
    fprintf(fp, "\n%10.3f",tp);
   
    fclose(fp);

    //printx(x, 8);
    printf("n = %d,  t = 1 process %10.3f sec\n", n, tp);
    return 0;
} 